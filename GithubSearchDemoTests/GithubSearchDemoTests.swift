//
//  GithubSearchDemoTests.swift
//  GithubSearchDemoTests
//
//  Created by PX Chen on 2021/8/5.
//

import XCTest
@testable import GithubSearchDemo

class GithubSearchDemoTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGitHubAPI() throws {
        let exp = XCTestExpectation()
        let api = GitHubAPI()
        
        api.searchUsers(withQuery: "17Media", pageNum: 1, sort: .followers, order: .asc) { users, total, error, errorMsg in
            guard let users = users else {
                return
            }
            if users.count > 0 {
                exp.fulfill()
            } else {
                XCTAssert(false, "can't get users from github correctly!")
            }
        }
        wait(for: [exp], timeout: 10)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
