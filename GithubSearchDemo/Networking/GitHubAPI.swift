//
//  GitHubAPI.swift
//  GithubSearchDemo
//
//  Created by PX Chen on 2021/8/5.
//

import Foundation

public enum SearchOrder: String {
    case asc
    case desc
}

public enum SearchUsersSort: String{
    case followers
    case repositories
    case joined
}

final class GitHubAPI {
    var session: URLSession
    let baseUrl = "https://api.github.com"

    public init() {
        self.session = URLSession(configuration: URLSessionConfiguration.default)
    }
    
    public init(session: URLSession) {
        self.session = session
    }
    
    /// Find user list via various criteria
    /// - Parameters:
    ///   - query: The query contains one or more search keywords and qualifiers.
    ///   - pageNum: Page number of the results to fetch.
    ///   - sort: Sorts the results of your query by number of followers or repositories, or when the person joined GitHub.
    ///   - order: Determines whether the first search result returned is the highest number of matches (desc) or lowest number of matches (asc). This parameter is ignored unless you provide sort.
    ///   - completion: called when API request finished.
    /// - Returns: an `URLSessionDataTask` object.
    @discardableResult

    func searchUsers(withQuery query: String,
                     pageNum: Int = 1,
                     sort: SearchUsersSort? = nil,
                     order: SearchOrder = .desc,
                     completion: @escaping ([GitHubUser]?, Int, Error?, String?) -> ()) -> URLSessionDataTask {
        let path = "/search/users"
        var parameters = [String : String]()
        parameters["q"] = query
        parameters["order"] = order.rawValue
        if let sort = sort {
            parameters["sort"] = sort.rawValue
        }
        parameters["page"] = "\(pageNum)"
        let request = URLRequest(url: url(with: path, parameters: parameters)!)
        let task = session.dataTask(with: request) { (data, _, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion([], 0, error, error.localizedDescription)
                    return
                }
                guard let data = data else {
                    completion([], 0, nil, "The data is empty.")
                    return
                }
                guard let response = try? JSONDecoder().decode(SearchUsersResponse.self, from: data) else {
                    if let otherErrorResponse = try? JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed) as? Dictionary<String, String> {
                        completion([], 0, nil, otherErrorResponse["message"])
                        return
                    }
                    completion([], 0, nil, "Unknown Error.")
                    return
                }
                completion(response.items, response.total_count, nil, nil)
            }
        }
        task.resume()
        return task
    }
    
    func url(with path:String, parameters:[String : String]) -> URL? {
        var retUrl = ""
        if parameters.count > 0 {
            retUrl.append("?")
            parameters.keys.forEach {
                guard let value = parameters[$0] else { return }
                retUrl.append("\($0)=\(value)&")
            }
            retUrl.removeLast()
        }
        return URL(string: self.baseUrl + path + retUrl)
    }
}
