//
//  UserCell.swift
//  GithubSearchDemo
//
//  Created by PX Chen on 2021/8/5.
//

import Foundation
import UIKit

final class UserCell : UICollectionViewCell {
    
    // MARK: IBOutlet

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    // MARK: Properties

    static let reuseIdentifier = "UserCell"

    // MARK: UICollectionViewCell

    override func awakeFromNib() {
        super.awakeFromNib()

        layer.borderWidth = 1.0
        layer.borderColor = UIColor.red.cgColor
    }

    // MARK: Convenience

    /**
     Configures the cell for display based on the model.
     
     - Parameters:
         - data: An optional `UserModel` object to display.
    */
    func configure(with data: UserModel?) {
        nameLabel.text = data?.login
        if let s = data?.avatarUrl, let url = URL(string: s) {
            avatarImageView.setImageUrl(url)
        } else {
            avatarImageView.image = nil
        }
    }
}
