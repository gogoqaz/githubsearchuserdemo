//
//  ViewController.swift
//  GithubSearchDemo
//
//  Created by PX Chen on 2021/8/5.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: IBOutlet

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBarContainer: UIView!
    
    // MARK: Properties

    private let searchController = UISearchController(searchResultsController: nil)
    private var data: [UserModel]?
    private let viewModel: UserListViewModel = UserListViewModel(networkingService: GitHubAPI())

    // MARK: UIViewController overrides

    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchBarContainer.addSubview(searchController.searchBar)
        searchController.searchBar.placeholder = NSLocalizedString("Search Github Users", comment: "")
        
        bindViewModel()
    }
    
    // MARK: private functions

    private func bindViewModel() {
        viewModel.isRefreshing = { loading in
            UIApplication.shared.isNetworkActivityIndicatorVisible = loading
        }
        viewModel.didUpdateUsers = { [weak self] users in
            guard let strongSelf = self else { return }
            strongSelf.data = users
            strongSelf.collectionView.reloadData()
        }
        viewModel.errorHandling = { [weak self] error in
            let alert = UIAlertController(title: "Error!!", message: error, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self?.searchController.present(alert, animated: true)
        }
    }
}

extension ViewController: UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data?.count ?? 0
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.frame.size.width * 0.8, height: 50)
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDataSourcePrefetching {
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        guard let item = indexPaths.last?.item else { return }
        viewModel.requestMoreUsers(from: item)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let user = data?[indexPath.item],
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: UserCell.reuseIdentifier , for: indexPath) as? UserCell
              else {
            return UICollectionViewCell()
        }
        
        cell.configure(with: user)
        return cell
    }
}

extension ViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.didChangeQuery(searchController.searchBar.text ?? "")
    }
}
