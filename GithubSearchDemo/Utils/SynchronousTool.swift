//
//  SynchronousTool.swift
//  GithubSearchDemo
//
//  Created by PX Chen on 2021/8/5.
//

import Foundation

class SynchronousTool {
    static func synced(_ lock: Any, closure: () -> ()) {
        objc_sync_enter(lock)
        closure()
        objc_sync_exit(lock)
    }
    
    static func asyncMainSafe(closure: @escaping () -> ()) {
        if Thread.isMainThread {
            closure()
        }
        else {
            DispatchQueue.main.async {
                closure()
            }
        }
    }
}
