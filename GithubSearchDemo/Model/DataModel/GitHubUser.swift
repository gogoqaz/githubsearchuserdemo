//
//  GitHubUser.swift
//  GithubSearchDemo
//
//  Created by PX Chen on 2021/8/5.
//

import Foundation

struct GitHubUser : Codable {
    public let login : String
    public let id : Int
    public let avatar_url : String
}
