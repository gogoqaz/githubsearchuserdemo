//
//  SearchUsersResponse.swift
//  GithubSearchDemo
//
//  Created by PX Chen on 2021/8/5.
//

import Foundation

struct SearchUsersResponse : Codable {
    public let incomplete_results : Bool
    public let items : [GitHubUser]
    public let total_count : Int
}
