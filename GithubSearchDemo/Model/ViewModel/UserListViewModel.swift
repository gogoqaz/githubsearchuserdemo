//
//  UserListViewModel.swift
//  GithubSearchDemo
//
//  Created by PX Chen on 2021/8/5.
//

import Foundation

struct UserModel {
    public let login : String?
    public let id : Int?
    public let avatarUrl : String?
}

extension UserModel {
    init(user: GitHubUser) {
        self.id = user.id
        self.login = user.login
        self.avatarUrl = user.avatar_url
    }
}

final class UserListViewModel {
    // MARK: Outputs
    
    var isRefreshing: ((Bool) -> Void)?
    var didUpdateUsers: (([UserModel]) -> Void)?
    var errorHandling: ((String?) -> Void)?
    
    // MARK: Properties

    private(set) var users: [GitHubUser] = [GitHubUser]() {
        didSet {
            didUpdateUsers?(users.map { UserModel(user: $0) })
        }
    }
    private(set) var totalUsers = 0;
    private(set) var currentPageNum = 0;
    
    private let throttle = Throttle(minimumDelay: 0.5)
    private var currentSearchNetworkTask: URLSessionDataTask?
    private var lastQuery: String?
    
    // MARK: Dependencies
    
    private let networkingService: GitHubAPI
    
    init(networkingService: GitHubAPI) {
        self.networkingService = networkingService
    }
    
    func requestMoreUsers(from index: Int) {
        guard let query = lastQuery else { return }
        SynchronousTool.synced(self) { [weak self] in
            guard let self = self else { return }
            guard currentPageNum * 30 <= (index + 1) && index < totalUsers else { return }
            
            currentPageNum += 1
            throttle.throttle {
                self.startSearchWithQuery(query, pageNum: self.currentPageNum)
            }
        }
    }
    
    func didChangeQuery(_ query: String) {
        guard query.count > 0, query != lastQuery else { return }
        SynchronousTool.synced(self) { [weak self] in
            guard let self = self else { return }
            
            currentPageNum = 0
            totalUsers = 0
            users = []
            lastQuery = query
            throttle.throttle {
                self.startSearchWithQuery(query)
            }
        }
    }
    
    // MARK: Privates
    
    private func startSearchWithQuery(_ query: String, pageNum: Int = 1) {
        print("query with page num \(pageNum)")
        currentPageNum = pageNum
        currentSearchNetworkTask?.cancel() // cancel previous pending request
        
        isRefreshing?(true)

        currentSearchNetworkTask = networkingService.searchUsers(withQuery: query, pageNum: currentPageNum) { [weak self] users, total, error, errorMsg in
            guard let strongSelf  = self else { return }
            if let errorMsg = errorMsg {
                strongSelf.currentPageNum -= 1
                strongSelf.finishSearching(with: errorMsg)
                return
            }
            strongSelf.totalUsers = total
            strongSelf.finishSearching(with: users)
        }
    }
    
    private func finishSearching(with errorMsg: String) {
        isRefreshing?(false)
        errorHandling?(errorMsg)
    }
    
    private func finishSearching(with users: [GitHubUser]?) {
        isRefreshing?(false)
        guard let users = users else { return }
        self.users.append(contentsOf: users)
    }
}


